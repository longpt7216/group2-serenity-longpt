@CreateNewArticle
Feature: i create a new article

  Background: i login to home page
    Given i login to home page

  @CreateNewArticleSuccessfully
  Scenario Outline: i create a new article successfully
    Given i go to "Articles" page
    When i create a new Article
    And i enter new article infomation of "<Title>"
    Then article is successfully created and "Article saved." message is display
    When i search for existing article and verify it's display correcly , and go to change display of this article
    Then i verify the infomation is correct
    Examples:
      | Title |
      | LongPT-Title02 |

  @CreateNewArticleButDoublicateAlias
  Scenario Outline: i create a new article but unsuccessfully
    Given i go to "Articles" page
    When i create a new Article
    And i enter new article infomation of "<Title>"
    Then article is unsuccessfully created and "Save failed with the following error: Another article from this category has the same alias (remember it may be a trashed item)." message is display
    Examples:
      | Title |
      | LongPT-Title03 |


#  //  mvn clean verify -Dwebdriver.driver="chrome"
#  //  mvn clean verify -Dwebdriver.driver="chrome" -Dcucumber.filter.tags="@CreateNewArticleSuccessfully"
#  //  mvn clean verify -Dwebdriver.driver="chrome" -Dcucumber.filter.tags="@CreateNewArticleButDoublicateAlias"
#  //  mvn clean verify -Dwebdriver.driver="chrome" -Dcucumber.filter.tags="@CreateNewArticleSuccessfully or @CreateNewArticleButDoublicateAlias"