package day1.DriverManager;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;

public class LocalDriver extends PageObject {
    public static WebDriver driver;

    public static WebDriver getLocalDriver() {
        return driver;
    }

    public void setLocalDriver(){
        driver = getDriver();
    }

    public static void quitDriver(){driver.quit();}

}
