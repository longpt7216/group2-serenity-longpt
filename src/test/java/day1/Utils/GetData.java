package day1.Utils;

import day1.objects.Article;

import java.io.IOException;

public class GetData { public static Article[] getData() throws IOException {return Util.deserializeJson(LoadConfig.getArticleDataPath(), Article[].class);}}
