package day1.Utils;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class LoadConfig {
    final static EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();

    public static String getDriver() {return EnvironmentSpecificConfiguration.from(env).getProperty("webdriver.driver");}

    public static String getBaseUrl() {
        return EnvironmentSpecificConfiguration.from(env).getProperty("baseUrl");
    }

    public static String getUsername() {return EnvironmentSpecificConfiguration.from(env).getProperty("account.username");}

    public static String getPassword() {return EnvironmentSpecificConfiguration.from(env).getProperty("account.password");}

    public static String getArticleDataPath() {return EnvironmentSpecificConfiguration.from(env).getProperty("ArticleDataPath");
    }

}
