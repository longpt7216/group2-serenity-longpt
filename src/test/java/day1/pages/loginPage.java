package day1.pages;

import day1.Utils.LoadConfig;
import day1.base.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;


public class loginPage extends BasePage{
    @FindBy(xpath = "//*[@id='mod-login-username']") private WebElementFacade E_USERNAME;
    @FindBy(xpath = "//*[@id='mod-login-password']") private WebElementFacade E_PASSWORD;


    @Step
    public loginPage enterAccount(){
        waits.Wait();
        E_USERNAME.sendKeys(LoadConfig.getUsername());
        E_PASSWORD.sendKeys(LoadConfig.getPassword());
        return this;
    }

    @Step
    public loginPage ClickLoginButton(){
        E_PASSWORD.submit();
        waits.Wait();
        return null;
    }
}
