package day1.pages;

import day1.objects.Article;
import day1.objects.ArticleStatus;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


import static org.assertj.core.api.Assertions.assertThat;

public class newArticlePage extends articlesPage {
    @FindBy(xpath = "//*[@id='jform_title']") private WebElementFacade TILTE_TEXTBOX;
    @FindBy(xpath = "//*[@id='jform_alias']") private WebElementFacade ALIAS_TEXTBOX;
    @FindBy(xpath = "//*[@id='tinymce']") private WebElementFacade ARTICLE_TEXTBOX;
    @FindBy(xpath = "//*[@id='jform_state']") private WebElementFacade ARTICLE_STATUS;
    @FindBy(xpath = "//*[@id='jform_state']/option[@selected='selected']") private WebElementFacade ARTICLE_STATUS_SELECTED;

    @FindBy(xpath = "//*[contains(text(),'Images and Links') and @role='tab']") private WebElementFacade IMAGEANDLINKTABBUTTON;
    @FindBy(xpath = "//*[@id='jform_images_image_intro']//following-sibling::button[ @class='btn btn-success button-select']") private WebElementFacade SELECTUPLOAD;
    @FindBy(xpath = "(//div[contains(@title,'.png')])[1]") private WebElementFacade INTROIMAGE;
    @FindBy(xpath = "//*[@class='btn btn-success button-save-selected']") private WebElementFacade SAVEINTROIMAGE;
    @FindBy(xpath = "//*[@class='button-save btn btn-success']") private WebElementFacade SAVEANDCLOSEBUTTON;


    private final String ARTICLE_TEXTBOX_IFRAME = "jform_articletext_ifr";
    private final String IMAGEANDLINK_IFRAME = "Change Image";


    @Step
    public newArticlePage enterTilteTextBox(String text) {
        super.enterTextIntoTxtBox(text, TILTE_TEXTBOX);
        return this;
    }

    @Step
    public newArticlePage enterAliasTextBox(String text) {
        super.enterTextIntoTxtBox(text, ALIAS_TEXTBOX);
        return this;
    }

    @Step
    public newArticlePage enterArticleTextBox(String text) {
        switchIFrame(ARTICLE_TEXTBOX_IFRAME);
        super.enterTextIntoTxtBox(text, ARTICLE_TEXTBOX);
        switchIFrame("parent");
        return this;
    }

    @Step
    public newArticlePage choseArticleStatus(ArticleStatus status) {
        choseDropdown(ARTICLE_STATUS, status.toString());
        return this;
    }


    @Step
    public newArticlePage choseImageAndLink() {
        waitUntilElementClickAbleAndClick(IMAGEANDLINKTABBUTTON);
        waitUntilElementClickAbleAndClick(SELECTUPLOAD);
        switchIFrame(IMAGEANDLINK_IFRAME);
        waitUntilElementClickAbleAndClick(INTROIMAGE);
        switchIFrame("parent");
        waitUntilElementClickAbleAndClick(SAVEINTROIMAGE);
        return this;
    }

    @Step
    public newArticlePage saveAndClose() {
        wait(1);
        waitUntilElementClickAbleAndClick(SAVEANDCLOSEBUTTON);
        waits.Wait();
        return this;
    }



    @Step
    public void verifyArticleOnNewArticlePage(Article article) {
        waits.Wait();
        //for color
        getElement(TILTE_TEXTBOX);
        getElement(ALIAS_TEXTBOX);
        getElement(ARTICLE_STATUS);

        assertThat(getDisplayingArticle()).isEqualToComparingFieldByField(article);
    }

    public Article getDisplayingArticle(){
        Article displayingArticle = new Article();
        displayingArticle.setTitle(TILTE_TEXTBOX.getAttribute("value"));
        displayingArticle.setAlias(ALIAS_TEXTBOX.getAttribute("value"));
        displayingArticle.setStatus(new Select(ARTICLE_STATUS).getFirstSelectedOption().getText());
        switchIFrame(ARTICLE_TEXTBOX_IFRAME);
        getElement(ARTICLE_TEXTBOX);
        displayingArticle.setArticleText(ARTICLE_TEXTBOX.getText());
        switchIFrame("parent");
        return displayingArticle;
    }


}
