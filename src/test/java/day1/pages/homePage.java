package day1.pages;

import day1.base.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;

public class homePage extends BasePage {
    @FindBy(xpath = "//*[@class='icon-file-alt']") private WebElementFacade E_ARTICLE;


    @Step
    public void clickAPageInHomePage(String page){
        switch (page) {
            case "Articles" :
                waitUntilElementClickAbleAndClick(E_ARTICLE);
                break;
            default:
                System.out.println("Page Option Not Exist");
        }
    }
}
