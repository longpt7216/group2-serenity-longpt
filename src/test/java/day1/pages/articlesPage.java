package day1.pages;

import day1.base.BasePage;
import day1.objects.Article;
import day1.objects.ArticleStatus;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;

import static org.assertj.core.api.Assertions.assertThat;

public class articlesPage extends BasePage {
    private WebElementFacade articleAccess;
    private WebElementFacade articleAuthor;


    @FindBy(xpath = "//*[@class='button-new btn btn-success']") private WebElementFacade NEWARTICLEBUTTON;
    @FindBy(xpath = "//div[@class='alert-message']") private WebElementFacade ALERTMESSAGE;
    @FindBy(xpath = "//*[@id='filter_search']") private WebElementFacade SEARCHBOX;
    @FindBy(xpath = "//div[@class='small break-word']/preceding-sibling::a[1]") private WebElementFacade TITLE;
    @FindBy(xpath = "//div[@class='small break-word']") private WebElementFacade ALIAS;


    public WebElementFacade getArticleAccess(String article){
        articleAccess = $("//*[contains(text(),'Alias: " + article + "')]//ancestor::th/following-sibling::td[@class='small d-none d-md-table-cell'][1]");
        return articleAccess;
    }

    public WebElementFacade getArticleAuthor(String article){
        articleAuthor = $("//*[contains(text(),'Alias: " + article + "')]//ancestor::th/following-sibling::td[@class='small d-none d-md-table-cell'][2]");
        return articleAuthor;
    }

    @Step
    public articlesPage clickCreateArticleButton(){
        waitUntilElementClickAbleAndClick(NEWARTICLEBUTTON);
        waits.Wait();
        return this;
    }

    @Step
    public String getFlashMessage(){
        waits.Wait();
        wait(1); // for firefox cus it's unstable than other
        return ALERTMESSAGE.getText();
    }

    public articlesPage searchArticle(String alias) {
        super.enterTextIntoTxtBox(alias, SEARCHBOX);
        SEARCHBOX.submit();
        return this;
    }

    @Step
    public articlesPage verifyArticleOnArticlePage(Article article) {
        waits.Wait();
        if (article.getStatus() == ArticleStatus.Published || article.getStatus() == ArticleStatus.Unpublished){
            String Alias = article.getAlias();
            // Verify
            getElement(TITLE); //for color
            getElement(ALIAS); //for color
            getElement(getArticleAccess(Alias)); //for color
            getElement(getArticleAuthor(Alias)); //for color

            assertThat(TITLE.getText()).isEqualTo(article.getTitle());
            assertThat(ALIAS.getText()).isEqualTo("Alias: " + article.getAlias());
            assertThat(getArticleAccess(Alias).getText()).isEqualTo("Public");
            assertThat(getArticleAuthor(Alias).getText()).isEqualTo("Thang Le");
        } else {
            System.out.println("The Article is not Published");
        }
        return this;
    }


    public articlesPage clickCreatedEarlyArticleButton() {
        wait(1); // for firefox cus it's unstable than other
        waitUntilElementClickAbleAndClick(TITLE);
        waits.Wait();
        return this;
    }
}
