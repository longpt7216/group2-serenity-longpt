package day1.base;

import day1.Utils.Util;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BasePage extends PageObject{
    public Util waits;

    public static void wait(int second) {
        try {
            Thread.sleep(second * 1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public WebElementFacade getElement(WebElementFacade element) {
        colorElement(element);
        element.waitUntilVisible();
        return element;
    }

    public void waitUntilElementClickAbleAndClick(WebElementFacade element) {
        element.waitUntilClickable();
        colorElement(element);
        element.click();

    }

    public void colorElement(WebElementFacade element){
        evaluateJavascript("arguments[0].setAttribute('style', 'background: orange; border: 2px solid red;');", element);
    }

    public void switchIFrame(String iframe){
        if(iframe.equals("parent")){
            waits.Wait();
            getDriver().switchTo().defaultContent();

        } else {
            waits.Wait();
            waitFor(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe));
        }
    }

    public void enterTextIntoTxtBox(String text, WebElementFacade element) {
        waits.Wait();
        colorElement(element);
        element.waitUntilClickable().type(text);
    }

    public void choseDropdown(WebElementFacade element, String text){
        colorElement(element);
        element.selectByVisibleText(text);
    }

}
