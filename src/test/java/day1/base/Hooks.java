package day1.base;

import day1.DriverManager.LocalDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;



public class Hooks {
    public static LocalDriver localDriver;
    public static String currentlyExecutingScenario;

    @Before
    public void setTheStage() {localDriver.setLocalDriver();}

    @After
    public void endTheAct() {LocalDriver.quitDriver();}

    @Before
    public void getCurrentlyExecutingScenario(Scenario scenario){currentlyExecutingScenario = scenario.getName();}
}
