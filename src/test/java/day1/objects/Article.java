package day1.objects;

public class Article {

    private String title;
    private String alias;
    private String articleText;
    private ArticleStatus status;

    public Article() {
    }

    public Article(String title, String alias, String articleText, String status) {
        this.title = title;
        this.alias = alias;
        this.articleText = articleText;
        this.status = ArticleStatus.valueOf(status);
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlias() {
        return alias.toLowerCase();
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getArticleText() {
        return articleText;
    }

    public void setArticleText(String articleText) {
        this.articleText = articleText;
    }

    public ArticleStatus getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = ArticleStatus.valueOf(status);
    }

//    @Override
//    public String toString() {
//        System.out.println(this.getTitle() + "\n" +
//                this.getAlias() + "\n" +
//                this.getArticleText() + "\n" +
//                this.getStatus() + "\n") ;
//        return null;
//    }
}
