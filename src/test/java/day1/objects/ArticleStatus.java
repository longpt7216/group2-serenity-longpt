package day1.objects;

public enum ArticleStatus {
    Published,
    Unpublished,
    Archived,
    Trashed
}
