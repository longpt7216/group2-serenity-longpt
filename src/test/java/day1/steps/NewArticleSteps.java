package day1.steps;

import day1.Utils.GetData;
import day1.objects.Article;
import day1.pages.newArticlePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

import static day1.Utils.Util.getCurrentTime;
import static day1.base.Hooks.currentlyExecutingScenario;

public class NewArticleSteps {

    @Steps
    static
    newArticlePage newArticlePage;


    public static
    GetData getData;

    public static Article newArticle;


    public static Article[] articles;

    static {
        try {
            articles = getData.getData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step
    public NewArticleSteps iGetDataFromJackSon(String title) {
        for (Article a : articles) {
            if (a.getTitle().equals(title)) {
                newArticle = a;
                //Fix random alias cho đỡ mất công sửa file
                if (currentlyExecutingScenario.equals("i create a new article successfully")) {newArticle.setAlias(newArticle.getAlias() + getCurrentTime());}
            }
        }
        return this;
    }


    @Step
    public NewArticleSteps iEnterNewArticleInfomationOf() {
        newArticlePage
                .enterTilteTextBox(newArticle.getTitle())
                .enterAliasTextBox(newArticle.getAlias())
                .choseArticleStatus(newArticle.getStatus())
                .enterArticleTextBox(newArticle.getArticleText())
                .choseImageAndLink()
                .saveAndClose();
        return this;
    }

    @Step
    public void iVerifyTheInfomationIsCorrect() { newArticlePage.verifyArticleOnNewArticlePage(newArticle);
    }
}
