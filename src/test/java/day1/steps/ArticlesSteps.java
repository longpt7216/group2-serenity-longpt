package day1.steps;

import day1.pages.articlesPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Assertions.assertThat;

public class ArticlesSteps {

    @Steps
    articlesPage articlesPage;

    @Step
    public ArticlesSteps iCreateANewArticle() {
        articlesPage.clickCreateArticleButton();
        return this;
    }

    @Step
    public ArticlesSteps iVerifyArticleIsSuccessfulCreatedAndMessageIsDisplay(String message) {
        assertThat(articlesPage.getFlashMessage()).isEqualTo(message);
        return this;
    }

    @Step
    public ArticlesSteps iSearchForExistingArticle() {
        articlesPage.searchArticle(NewArticleSteps.newArticle.getAlias());
        return this;

    }

    @Step
    public ArticlesSteps verifyArticleOnArticlePage() {
        articlesPage.verifyArticleOnArticlePage(NewArticleSteps.newArticle);
        return this;

    }

    @Step
    public ArticlesSteps clickCreatedEarlyArticleButton() {
        articlesPage.clickCreatedEarlyArticleButton();
        return this;
    }
}
