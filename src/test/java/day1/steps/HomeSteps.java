package day1.steps;

import day1.pages.homePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class HomeSteps {

    @Steps
    homePage homePage;

    @Step
    public void goToPage(String page){
        homePage.clickAPageInHomePage(page);
    }
}
