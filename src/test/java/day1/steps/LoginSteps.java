package day1.steps;

import day1.pages.loginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class LoginSteps {
    @Steps
    loginPage loginPage;

    @Step
    public void iLoginToHomePage() {
        loginPage.open();
        loginPage.enterAccount()
                .ClickLoginButton();
    }



}
