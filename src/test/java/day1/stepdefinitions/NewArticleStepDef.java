package day1.stepdefinitions;

import day1.steps.NewArticleSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;


public class NewArticleStepDef extends ScenarioSteps {


    @Steps
    NewArticleSteps newArticleSteps;


    @And("i enter new article infomation of {string}")
    public void iEnterNewArticleInfomationOf(String title) {
        newArticleSteps
                .iGetDataFromJackSon(title)
                .iEnterNewArticleInfomationOf();
    }

    @Then("i verify the infomation is correct")
    public void iVerifyTheInfomationIsCorrect() { newArticleSteps.iVerifyTheInfomationIsCorrect();}
}
