package day1.stepdefinitions;


import day1.steps.LoginSteps;
import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class LoginStepDef extends ScenarioSteps {


    @Steps
    LoginSteps loginSteps;

    @Given("i login to home page")
    public void iLoginToHomePage() {
        loginSteps.iLoginToHomePage();
    }
}
