package day1.stepdefinitions;

import day1.steps.ArticlesSteps;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class ArticlesStepDef extends ScenarioSteps {


    @Steps
    ArticlesSteps articlesSteps;

    @Then("i create a new Article")
    public void iCreateANewArticle() {
        articlesSteps.iCreateANewArticle();
    }

    @Then("article is successfully/unsuccessfully created and {string} message is display")
    public void iVerifyArticleIsSuccessfulCreatedAndMessageIsDisplay(String message) {
        articlesSteps.iVerifyArticleIsSuccessfulCreatedAndMessageIsDisplay(message);
    }

    @When("i search for existing article and verify it's display correcly , and go to change display of this article")
    public void iSearchForExistingArticle() {
        articlesSteps
                .iSearchForExistingArticle()
                .verifyArticleOnArticlePage()
                .clickCreatedEarlyArticleButton();
    }
}
